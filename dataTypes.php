<?php
//String
//$x = "Hello world!";
//$y = 'Hello world!';
//
//echo $x;
//echo "<br>";
//echo $y;

//Integer
//$x = 5985;
//var_dump($x);

//PHP Float
//$x = 10.365;
//var_dump($x);

//PHP Boolean
//$x = true;
//$y = false;

//PHP Array
//$cars = array("Volvo","BMW","Toyota");
//var_dump($cars);

//PHP Object
//class Car {
//    public $color;
//    public $model;
//    public function __construct($color, $model) {
//        $this->color = $color;
//        $this->model = $model;
//    }
//    public function message() {
//        return "My car is a " . $this->color . " " . $this->model . "!";
//    }
//}
//
//$myCar = new Car("black", "Volvo");
//echo $myCar -> message();
//echo "<br>";
//$myCar = new Car("red", "Toyota");
//echo $myCar -> message();

//PHP NULL value
//$x = "Hello world!";
//$x = null;
//var_dump($x);

//PHP string function
//echo strlen("Hello world!"); // outputs 12

//PHP String Word Count
//echo str_word_count("Hello world!"); // outputs 2

//PHP Revese a String
//echo strrev("Hello world!"); // outputs !dlrow olleH

//PHP Search Position of a word in a string
//echo strpos("Hello world!", "world"); // outputs 6

//PHP Replace word in sentence
echo str_replace("world", "Dolly", "Hello world!"); // outputs Hello Dolly!
?>