<?php
//PHP Check if it is an Integer
//$x = 5985;
//var_dump(is_int($x));
//
//$x = 59.85;
//var_dump(is_int($x));

//PHP Check if it is a Float
//$x = 10.365;
//var_dump(is_float($x));

//PHP Check if it is an Infinity
//$x = 1.9e411;
//var_dump($x);

//PHP Check if it is a NAN
//$x = acos(8);
//var_dump($x);
//
////PHP Check if it is a Numerical String
//$x = 5985;
//var_dump(is_numeric($x));
//
//$x = "5985";
//var_dump(is_numeric($x));
//
//$x = "59.85" + 100;
//var_dump(is_numeric($x));
//
//$x = "Hello";
//var_dump(is_numeric($x));

//PHP Casting Strings and Float to Integer
// Cast float to int
//$x = 23465.768;
//$int_cast = (int)$x;
//echo $int_cast;
//
//echo "<br>";
//
//// Cast string to int
//$x = "23465.768";
//$int_cast = (int)$x;
//echo $int_cast;

//PHP pi function
//echo(pi()); // returns 3.1415926535898

//PHP min() and max() function
//echo min((0, 150, 30, 20, -8, -200);  // returns -200
//echo(max(0, 150, 30, 20, -8, -200));  // returns 150

//PHP abs() function
//echo(abs(-6.7));  // returns 6.7

//PHP sqrt() function
//echo sqrt(68);

//PHP round() function
//echo(round(0.60));  // returns 1
//echo(round(0.49));  // returns 0

//PHP rand()
//echo(rand()); //10 digits
echo(rand(10, 100)); //range
?>