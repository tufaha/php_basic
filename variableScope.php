<?php

//$x = 5; // global scope
//
//function myTest()
//{
//    // using x inside this function will generate an error
//    echo "<p>Variable x inside function is: $x<br><br/>></p>";
//    //echo "<br/>";
//}
//myTest();
//
//echo "<p>Variable x outside function is: $x</p>";

//function myTest2()
//{
//    $x = 5; // local scope
//    echo "<p>Variable x inside function is: $x</p>";
//}
//
//myTest2();
//
//// using x outside the function will generate an error
//echo "<p>Variable x outside function is: $x</p>";

//PHP the Global Keyword
//$x = 5;
//$y = 10;
//
//function myTest() {
//    global $x, $y;
//    $y = $x + $y;
//}
//
//myTest();
//echo $y; // outputs 15

//$x = 5;
//$y = 10;
//
//function myTest() {
//    $GLOBALS['y'] = $GLOBALS['x'] + $GLOBALS['y'];
//}
//
//myTest();
//echo $y; // outputs 15

//PHP static keyword
function myTest() {
    static $x = 0;
    echo $x;
    $x++;
}

myTest();
echo "<br>";
myTest();
echo "<br>";
myTest();
?>

